import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class View {

  private Model model;
  private Stage primaryStage;
  private Scene scene;
  private BorderPane borderPane;
  private Button restartButton;

  TextArea[][] textAreas = new TextArea[9][9];
  TextArea textArea;

  public View(Model model, Stage primaryStage) {

    this.model = model;
    this.primaryStage = primaryStage;

    restartButton = new Button("Restart");

    GridPane largeGrid = new GridPane();
    largeGrid.setAlignment(Pos.CENTER);
    borderPane = new BorderPane();

    /**
     * Ein großes 3x3 Grid, jedes Feld enthält jeweils ein kleines 3x3 Grid
     */
    for (int blockRow = 0; blockRow < 3; blockRow++) {
      for (int blockColumn = 0; blockColumn < 3; blockColumn++) {

        GridPane smallGrid = new GridPane();
        smallGrid.setGridLinesVisible(true);

        for (int fieldRow = 0; fieldRow < 3; fieldRow++) {
          for (int fieldColumn = 0; fieldColumn < 3; fieldColumn++) {
            int row = (blockRow * 3) + fieldRow;
            int column = (blockColumn * 3) + fieldColumn;
            textArea = new TextArea();
            textArea.setPromptText(String.valueOf(model.getPossibleNumbers(row,column))); //Liste der möglichen Zahlen als Prompt
            textAreas[row][column] = textArea;
            smallGrid.add(textArea, fieldColumn, fieldRow);
          }
        }
        largeGrid.add(smallGrid, blockColumn, blockRow);
      }
    }
    /**
     * Layout
     */
    borderPane.setCenter(largeGrid);
    borderPane.setBottom(restartButton);
    scene = new Scene(borderPane);
    primaryStage.setScene(scene);
    primaryStage.setTitle("Sudoku Helfer");
    primaryStage.setWidth(510);
    primaryStage.setHeight(530);
    primaryStage.setResizable(false);

    update();
  }

  /**
   * Geht durch das SudokuGrid und füllt Felder entweder mit dem Wert, mit dem vorgeschlagenen Wert,
   * oder der Liste der möglichen Zahlen.
   */
  void update() {
    model.solve();
    for (int row = 0; row < 9; row++) {
      for (int column = 0; column < 9; column++) {


        int value = model.getValue(row, column);
        int suggested = model.getSuggestedValue(row,column);

        if (value != 0) { // Wert ist festgelegt
          textAreas[row][column].setText(String.valueOf(value));
        } else if (suggested != 0){ // Es gibt nur einen möglichen Wert
          textAreas[row][column].setText(String.valueOf(suggested));
        } else { // Es gibt mehrere mögliche Werte
          StringBuilder possibleNumbers = new StringBuilder();
          for (int number : model.getPossibleNumbers(row, column)) {
            possibleNumbers.append(String.valueOf(number));
          }
          textAreas[row][column].setPromptText(String.valueOf(possibleNumbers));
        }
      }
    }
  }

  Button getRestartButton() {
    return restartButton;
  }


  TextArea getTextArea(int row, int column) {
    return textAreas[row][column];
  }


  public Stage getPrimaryStage() {
    return primaryStage;
  }
}

