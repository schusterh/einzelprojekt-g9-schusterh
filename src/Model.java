import javafx.beans.InvalidationListener;
import javafx.beans.property.*;
import javafx.beans.Observable;

import java.util.ArrayList;
import java.util.Arrays;

public class Model {


    class SudokuField {
        private ArrayList<Integer> possibleNumbers;
        private int value;
        private int suggestedValue;
        private final int row;
        private final int column;


        /* Konstruktor Sudokufeld */
        SudokuField(int row, int column) {
            this.possibleNumbers = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9));
            this.row = row;
            this.column = column;
            this.suggestedValue = 0;
            this.value = 0;
        }
    }

    private final SudokuField[][] grid = new SudokuField[9][9];
    private boolean res;

    /* Konstruktor Model */
    public Model(){
        for (int i = 0; i < 9; i++){
            for (int j = 0; j < 9; j++)
                grid[i][j] = new SudokuField(i,j);
        }

    }

    /**
     * Zum Lösen des Sudokus wird durch die einzelnen Felder iteriert, bis ein
     * leeres Feld gefunden wird. Dann werden die möglichen Zahlen (1...9)
     * in den jeweilig zugehörigen Reihen, Spalten und 3x3 Blöcken gesucht und
     * aus der Liste der möglichen Zahlen gelöscht.
     * Wenn diese Liste nur aus einem Element besteht, wird dieses zum vorgeschlagenen
     * Wert und beeinflusst wiederum die möglichen Zahlen benachbarter Zellen.
     */

    public void solve(){
        for (int gridRow = 0; gridRow < 9; gridRow++){
            for (int gridColumn = 0; gridColumn < 9; gridColumn++){
                if (grid[gridRow][gridColumn].value == 0){

                    SudokuField emptyField = grid[gridRow][gridColumn];

                    for (int checkNumber = 1; checkNumber < 10; checkNumber++){
                        boolean result;
                        if (isInColumn(emptyField,checkNumber) || isInRow(emptyField,checkNumber) || isInSmallGrid(emptyField,checkNumber))
                            result = true;


                        if (res)
                            emptyField.possibleNumbers.remove(Integer.valueOf(checkNumber));
                    }

                    if (emptyField.possibleNumbers.size() == 1){
                        grid[gridRow][gridColumn].suggestedValue = (emptyField.possibleNumbers.get(0));
                        solve();
                    }
                }
            }
        }
    }


    /* Überprüfung der Reihe */
    public boolean isInRow(SudokuField field, int number){
        for (int i = 0; i < 9; i++)
            res = grid[field.row][i].value == number;
        return res;
    }

    /* Überprüfung der Spalte */
    public boolean isInColumn(SudokuField field, int number){
        for (int i = 0; i < 9; i++)
            res = grid[i][field.column].value == number;
        return res;
    }

    /* Überprüfung des 3x3 Blocks*/
    public boolean isInSmallGrid(SudokuField field, int number){
        for (int i = 0; i < 9 && i/3 == field.row/3;i++){
            for (int j = 0; j < 9 && j/3 == field.column/3;j++)
                res = grid[i][j].value == number;
        }
        return res;
    }

    /* GETTER METHODEN */
    public ArrayList<Integer> getPossibleNumbers(int row, int column){
        return grid[row][column].possibleNumbers;
    }

    public int getValue(int row, int column){
        return grid[row][column].value;
    }
    public int getSuggestedValue(int row, int column){
        return grid[row][column].suggestedValue;
    }

    /* Alle Felder auf 0 */
    public void setEmpty(){
        for (int i = 0; i < 9; i++){
            for (int j = 0; j < 9; j++)
                grid[i][j] = new SudokuField(i,j);
        }
        solve();
    }

    public void setValue(int row, int column, int newVal) {
        grid[row][column].value = newVal;
        solve();
    }



}
