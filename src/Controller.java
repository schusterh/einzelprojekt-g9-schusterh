public class Controller {
    private View view;
    private Model model;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;

        view.getRestartButton().setOnAction(event -> restart());

        /* Wenn eine Zahl eingetragen wird, wird diese als Wert im SudokuGrid eingetragen. */
        for (int i = 0; i < 9; i++){
            for (int j = 0; j < 9; j++){
                int row = i;
                int column = j;
                view.getTextArea(row, column).setOnKeyTyped(event -> { updateValue(row,column,event.getCharacter()); });
            }
        }
        view.getPrimaryStage().show();
    }


    /**
     * alle Werte werden auf Null gesetzt
     */
    private void restart() {
        model.setEmpty();
        view.update();
    }

    /**
     * Eingegebener Wert newValue -> Model
     * @param row
     * @param column
     * @param newValue
     */
    private void updateValue(int row, int column, String newValue){
        model.setValue(row,column,Integer.parseInt(newValue));
        view.update();
    }

}